# pr_mod

With this program you will be able to dynamically visualize 
in your navigator the modifications into your PR.

- 0 dependencies
- Windows/Linux/Mac
- fast

```bash
cargo install pr_mod
```

## Usage

In your git project:

```bash
pr_mod
```

Navigate to the respective repository, run `pr_mod`, and it will open an HTML file in the localhost browser with all the detailed information about the pull request.

<div align="center">

<img  
alt="pr_mod"
src="https://gitlab.com/mateolafalce/pr_mod/-/raw/84a2aa63cdb104917fcc20d53f3ff78fdd9a002b/pr_mod.png" 
width="500" 
height="350" 
/>

</div>
