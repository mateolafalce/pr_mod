/***************************************************************************************
 *   client.rs  --  This file is part of pr_mod.                                       *
 *                                                                                     *
 *   Copyright (C) 2024 Mateo Lafalce                                                  *
 *                                                                                     *
 *   pr_mod is free software: you can redistribute it and/or modify                    *
 *   it under the terms of the GNU General Public License as published                 *
 *   by the Free Software Foundation, either version 3 of the License,                 *
 *   or (at your option) any later version.                                            *
 *                                                                                     *
 *   pr_mod is distributed in the hope that it will be useful,                         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty                       *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                           *
 *   See the GNU General Public License for more details.                              *
 *                                                                                     *
 *   You should have received a copy of the GNU General Public License                 *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.               *
 *                                                                                     *
 **************************************************************************************/

use crate::consts::CSS;
use std::{
    io::{Read, Result, Write},
    net::TcpStream,
};

/// set the html into a http response for the localhost
pub fn handle_client(mut stream: TcpStream, content: String) -> Result<()> {
    let html: String = content.to_owned() + CSS;
    let mut buffer: [u8; 1024] = [0; 1024];
    stream.read(&mut buffer)?;
    let response: String = format!("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n{}", html);
    stream.write(response.as_bytes())?;
    Ok(())
}
