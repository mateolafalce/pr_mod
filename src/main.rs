/***************************************************************************************
 *   main.rs  --  This file is part of pr_mod.                                         *
 *                                                                                     *
 *   Copyright (C) 2024 Mateo Lafalce                                                  *
 *                                                                                     *
 *   pr_mod is free software: you can redistribute it and/or modify                    *
 *   it under the terms of the GNU General Public License as published                 *
 *   by the Free Software Foundation, either version 3 of the License,                 *
 *   or (at your option) any later version.                                            *
 *                                                                                     *
 *   pr_mod is distributed in the hope that it will be useful,                         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty                       *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                           *
 *   See the GNU General Public License for more details.                              *
 *                                                                                     *
 *   You should have received a copy of the GNU General Public License                 *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.               *
 *                                                                                     *
 **************************************************************************************/

#![allow(clippy::unused_io_amount)]
use std::{
    env::{consts::OS, current_dir},
    io::Result,
    net::TcpListener,
    process::{exit, Command},
    thread::spawn,
};

/// The client Handler for the text/html static page
pub mod client;
/// Some usefull const
pub mod consts;
/// All process text operations & structs
pub mod process_text;

use crate::{
    client::handle_client,
    consts::{EMPTY, LOCALHOST, URL},
    process_text::{process_directory, Html},
};

/// Verify & get the dir ->
/// Then, add the title (dir name) in the html ->
/// Push all modifications into html ->
/// pPop up a web browser to show the modifications
pub fn main() -> Result<()> {
    let mut html: Html = Html::new();
    if let Ok(current_dir) = current_dir() {
        if let Some(dir_name) = current_dir.file_name() {
            let dir_name_str = dir_name.to_string_lossy();
            let title = dir_name_str.to_string();
            // add the title into html
            html.add_title(title);
        } else {
            eprintln!("Error: Getting the dir");
            std::process::exit(1);
        }
    } else {
        eprintln!("Error: Its not a dir");
        std::process::exit(1);
    }
    // push modifications into html
    let modifications: String = process_directory().content;
    if modifications.is_empty() {
        html.content.push_str(EMPTY);
    } else {
        html.content.push_str(&modifications);
    }
    // match web browser pop up
    let open_a_window_in_your_browser = match OS {
        "linux" | "macos" => "xdg-open",
        "windows" => "start",
        _ => {
            eprintln!("OS incompatible");
            exit(1);
        }
    };
    Command::new(open_a_window_in_your_browser)
        .arg(URL)
        .output()?;
    let listener = TcpListener::bind(LOCALHOST)?;
    // if all is OK, run the localhost, when it is received then end the process
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let html = html.clone();
                spawn(move || {
                    handle_client(stream, html.content).expect("Error with the local client");
                    exit(1);
                });
            }
            Err(e) => eprintln!("Error: {}", e),
        }
    }
    Ok(())
}
