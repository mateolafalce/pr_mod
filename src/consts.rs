/***************************************************************************************
 *   consts.rs  --  This file is part of pr_mod.                                       *
 *                                                                                     *
 *   Copyright (C) 2024 Mateo Lafalce                                                  *
 *                                                                                     *
 *   pr_mod is free software: you can redistribute it and/or modify                    *
 *   it under the terms of the GNU General Public License as published                 *
 *   by the Free Software Foundation, either version 3 of the License,                 *
 *   or (at your option) any later version.                                            *
 *                                                                                     *
 *   pr_mod is distributed in the hope that it will be useful,                         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty                       *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                           *
 *   See the GNU General Public License for more details.                              *
 *                                                                                     *
 *   You should have received a copy of the GNU General Public License                 *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.               *
 *                                                                                     *
 **************************************************************************************/

/// Link of favicon
pub const FAVICON_LINK: &str =
    "https://cdn0.iconfinder.com/data/icons/phosphor-fill-vol-3/256/git-branch-fill-512.png\">";
/// Favicon import html struct
pub const FAVICON: &str = "<link rel=\"icon\" type=\"image/x-icon\" href=\"";

/// CSS for red & green lines
pub const CSS: &str = "    
<style>
    .line-red {
         background: red;
         color: black;
    }
    .line-green {
        background: green;
        color: white;
    }
    div {
        margin-bottom: 90px;
    }
    p {
        padding: 0;
        margin: 0;
        white-space: pre;
        font-family: \"Lucida Console\", \"Courier New\", monospace;
    }
</style>
";

/// Template when theres is no change
pub const EMPTY: &str = "
    <h2>No data / modifications :(</h2>
    <p>It occurs because the actual branches have no changes or the .git folder doesn't exist</p>
";

/// local host url for listener
pub const LOCALHOST: &str = "127.0.0.1:8080";

/// local host url for command
pub const URL: &str = "http://127.0.0.1:8080";
