/***************************************************************************************
 *   process_text.rs  --  This file is part of pr_mod.                                 *
 *                                                                                     *
 *   Copyright (C) 2024 Mateo Lafalce                                                  *
 *                                                                                     *
 *   pr_mod is free software: you can redistribute it and/or modify                    *
 *   it under the terms of the GNU General Public License as published                 *
 *   by the Free Software Foundation, either version 3 of the License,                 *
 *   or (at your option) any later version.                                            *
 *                                                                                     *
 *   pr_mod is distributed in the hope that it will be useful,                         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty                       *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                           *
 *   See the GNU General Public License for more details.                              *
 *                                                                                     *
 *   You should have received a copy of the GNU General Public License                 *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.               *
 *                                                                                     *
 **************************************************************************************/

use crate::consts::{FAVICON, FAVICON_LINK};
use std::process::Command;

/// HTML struct
#[derive(Clone)]
pub struct Html {
    /// html content
    pub content: String,
}

/// process the raw content received -> Html
pub fn process_directory() -> Html {
    let mut html: Html = Html::new();
    let output = Command::new("git").arg("diff").output();
    let result = output.unwrap().stdout;
    let stdout_str: String = String::from_utf8(result).unwrap();
    if !stdout_str.is_empty() {
        // start a div for the modified file
        html.content = "<div>".to_string();
        for line in stdout_str.lines() {
            // if exist an adition, print green
            // if exist an delete, print red
            // else, print the line witout color
            let line_char: Vec<char> = line.chars().collect();
            if (line_char[0] == '+' && line.len() >= 2 && line_char[1] != '+')
                || line_char[0] == '+' && line.len() == 1
            {
                let mut modification = line.to_string();
                modification.remove(0); // remove '+' at start
                modification.insert(0, ' ');
                if modification.is_empty() {
                    modification.push(' ')
                }
                html.add_line_green(&modification);
            } else if (line_char[0] == '-' && line.len() >= 2 && line_char[1] != '-')
                || line_char[0] == '-' && line.len() == 1
            {
                let mut modification = line.to_string();
                modification.remove(0); // remove '-' at start
                modification.insert(0, ' ');
                if modification.is_empty() {
                    modification.push(' ')
                }
                html.add_line_red(&modification);
            } else {
                html.add_line(line);
            }
        }
        html.content.push_str("</div>");
        return html;
    }
    html
}

impl Default for Html {
    fn default() -> Self {
        Self::new()
    }
}

impl Html {
    /// New HTML struct
    pub fn new() -> Self {
        Self {
            content: String::new(),
        }
    }
    /// Add Title & favicon
    pub fn add_title(&mut self, name: String) {
        self.content.push_str("<head>");
        self.content.push_str("<title>");
        self.content.push_str(&name);
        self.content.push_str("</title>");
        self.content.push_str(FAVICON);
        self.content.push_str(FAVICON_LINK);
        self.content.push_str("</head>");
    }
    fn add_line_green(&mut self, modification: &str) {
        self.content.push_str("<p class=\"line-green\">");
        self.content.push_str(modification);
        self.content.push_str("</p>")
    }
    fn add_line_red(&mut self, modification: &str) {
        self.content.push_str("<p class=\"line-red\">");
        self.content.push_str(modification);
        self.content.push_str("</p>")
    }
    fn add_line(&mut self, line: &str) {
        self.content.push_str("<p>");
        self.content.push_str(line);
        self.content.push_str("</p>");
    }
}
